import { Request, Response } from 'express'
import { createBreed, listBreed } from '../repository/breed'
import { CreateBreedSchema, ListBreedSchema } from '../schemas/breed'
import { buildBreed } from '../utils'

export const addBreedHandler = async (req: Request<{}, {}, CreateBreedSchema['body']>, res: Response): Promise<void> => {
  try {
    const newBreed = buildBreed(req.body)
    const breed = await createBreed(newBreed)
    res.status(201).json(breed)
  } catch (e) {
    res.status(400).json({ message: e.message })
  }
}

export const listBreedHandler = async (req: Request<{}, {}, ListBreedSchema['query']>, res: Response): Promise<void> => {
  try {
    const list = await listBreed(+(req.query.page ?? 1), +(req.query.limit ?? 10))
    res.status(200).json(list)
  } catch (e) {
    res.status(400).json({ message: e.message })
  }
}

export const listBreedNamesHandler = (_req: Request, res: Response): void => {
  res.status(200).json([
    'affenpinscher',
    'african',
    'airedale',
    'akita',
    'appenzeller',
    'australian',
    'basenji',
    'beagle',
    'bluetick',
    'borzoi',
    'bouvier',
    'boxer',
    'brabancon',
    'briard',
    'buhund',
    'bulldog',
    'bullterrier',
    'cattledog',
    'chihuahua',
    'chow',
    'clumber',
    'cockapoo',
    'collie',
    'coonhound',
    'corgi',
    'cotondetulear',
    'dachshund',
    'dalmatian',
    'dane',
    'deerhound',
    'dhole',
    'dingo',
    'doberman',
    'elkhound',
    'entlebucher',
    'eskimo',
    'finnish',
    'frise',
    'germanshepherd',
    'greyhound',
    'groenendael',
    'havanese',
    'hound',
    'husky',
    'keeshond',
    'kelpie',
    'komondor',
    'kuvasz',
    'labradoodle',
    'labrador',
    'leonberg',
    'lhasa',
    'malamute',
    'malinois',
    'maltese',
    'mastiff',
    'mexicanhairless',
    'mix',
    'mountain',
    'newfoundland',
    'otterhound',
    'ovcharka',
    'papillon',
    'pekinese',
    'pembroke',
    'pinscher',
    'pitbull',
    'pointer',
    'pomeranian',
    'poodle',
    'pug',
    'puggle',
    'pyrenees',
    'redbone',
    'retriever',
    'ridgeback',
    'rottweiler',
    'saluki',
    'samoyed',
    'schipperke',
    'schnauzer',
    'setter',
    'sheepdog',
    'shiba',
    'shihtzu',
    'spaniel',
    'springer',
    'stbernard',
    'terrier',
    'tervuren',
    'vizsla',
    'waterdog',
    'weimaraner',
    'whippet',
    'wolfhound'
  ])
}
