import connection from './mongo.config'
import mongoose from 'mongoose'

export const connect = async (): Promise<void> => {
  try {
    await mongoose.connect(connection.connectionString ?? '')
  } catch (e) {
    console.error('Could not connect to DB')
    process.exit(1)
  }
}

export const disconnect = async (): Promise<void> => {
  console.log(1)
  await mongoose.disconnect()
  await mongoose.connection.close()
}
