import mongoose, { PaginateModel } from 'mongoose'
import mongoosePagination from 'mongoose-paginate-v2'
import { Breed } from '../types'

const breedSchema = new mongoose.Schema({
  breed: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  }
}, {
  timestamps: false,
  collection: 'Breed'
})

breedSchema.plugin(mongoosePagination)

const BreedModel = mongoose.model<Breed, PaginateModel<Breed>>('Breed', breedSchema)

export { BreedModel }
