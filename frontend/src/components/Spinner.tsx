import React from 'react'

const Spinner = () => {
  return (
    <div className='spinner-container'>
      <div className="spinner-border text-light m-5" role="status">
      </div>
    </div>
  )
}

export default Spinner
