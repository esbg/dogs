import { Document } from 'mongoose'
import { inject, injectable } from 'inversify'
import { GenericRepository } from './genericRepository'
import { Types } from '../../ioc/types'
import { DbClient } from './mongoConfig'
import { Breed } from '../../core/entities'

export interface BreedModel extends Breed, Document { }

@injectable()
export class BreedRepository extends GenericRepository<BreedModel> {
  public constructor(
    @inject(Types.types.dbClient) dbClient: DbClient
  ){
    super(
      dbClient,
      'Breed',
      {
        breed: {
          type: String,
          required: true
        },
        url: {
          type: String,
          required: true
        }
      }
    )
  }
}