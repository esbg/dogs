import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import Context from '../context/Context'
import Spinner from './Spinner'

const Header = () => {
  const { loading } = useContext(Context)

  return (
    <>
      {loading ? <Spinner /> :
        <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
            <Link to='/' className='navbar-brand btn'>Dogs</Link>
        <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
          <span className='navbar-toggler-icon'></span>
        </button>
        <div className='navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav mr-auto'>
            <li className='nav-item'>
            <Link to='/' className='nav-link'>Home</Link>
            </li>
            <li className='nav-item'>
            <Link to='/add' className='nav-link' >Add breed</Link>
            </li>
            <li className='nav-item'>
            <Link to='/list' className='nav-link' >List breeds</Link>
            </li>
          </ul>
        </div>
      </nav>
      }
    </>
  )
}

export default Header
