import { useEffect, useState } from 'react'

const useNearScreen = ({ distance = '50px', externalRef }: any = {}) => {
  const [isNearScreen, setIsNearScreen] = useState(false)

  useEffect(() => {
    const onChange = (entries: IntersectionObserverEntry[], observer: IntersectionObserver) => setIsNearScreen(entries[0].isIntersecting)
    const observer: IntersectionObserver = new IntersectionObserver(onChange, { rootMargin: distance })
    if(externalRef) observer.observe(externalRef.current)
    return () => observer && observer.disconnect()
  })

  return { isNearScreen }
}

export default useNearScreen
