import { createContext, useState } from 'react'
import { ContextType } from '../types'

const Context = createContext<ContextType>({ loading: false, setLoading: () => null })

export const Provider = ({ children }: any) => {
  const [loading, setLoading] = useState(false)

  return <Context.Provider value={{ loading, setLoading }}>
    {children}
  </Context.Provider>
}

export default Context
