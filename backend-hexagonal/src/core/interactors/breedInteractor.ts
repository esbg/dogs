import { IGenericRepository } from '../repositories'
import { Breed } from '../entities'
import { inject, injectable } from 'inversify'
import { Types } from '../../ioc/types'
import { IBreedService } from '../services'
import { BreedCreationDto, BreedListDto, BreedListEntity } from '../dtos'

@injectable()
export class BreedInteractor {
  constructor(@inject(Types.types.breedRepository) private breedRepository: IGenericRepository<Breed>,
    @inject(Types.types.breedService) private breedService: IBreedService) {
  }

  create = async (breed: Breed): Promise<BreedCreationDto> => {
    const createdBreed:Breed = await this.breedRepository.create(breed)
    return new BreedCreationDto(createdBreed)
  }

  list = async(page: number, limit: number): Promise<BreedListDto> => {
    const list: BreedListEntity = await this.breedRepository.list(page, limit)
    return new BreedListDto(list.hasNextPage, list.list)
  }

  names = async(): Promise<any> => this.breedService.names()
}