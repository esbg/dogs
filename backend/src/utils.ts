import { Breed } from './types'

export const buildBreed = (attrs: any): Breed => {
  return {
    breed: attrs.breed,
    url: attrs.url
  }
}
