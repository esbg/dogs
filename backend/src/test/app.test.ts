import request from 'supertest'
import app from '../app'
import { BreedModel } from '../model/breed'
import { connect, disconnect } from '../database/db.mongo.config'

beforeAll(async () => {
  await connect()
  await BreedModel.deleteMany({})
})

afterAll(async () => await disconnect())

describe('POST /breed', () => {
  test('Should create a breed', async () => {
    const response = await request(app)
      .post('/breed')
      .send({ breed: 'spaniel-sussex', url: 'https://images.dog.ceo/breeds/spaniel-sussex/n02102480_267.jpg' })

    expect(response.statusCode).toBe(201)
    expect(response.body.breed).toBeDefined()
    expect(response.body.url).toBeDefined()
  })

  test('Should validate url not empty', async () => {
    const response = await request(app)
      .post('/breed')
      .send({ breed: 'spaniel-sussex', url: '' })

    expect(response.statusCode).toBe(400)
    expect(response.body.message).toContain('`url` is required')
  })
})

describe('GET /breed?page={page}&limit={limit}', () => {
  test('Should get first page of breeds list', async () => {
    const response = await request(app)
      .get('/breed')
      .query({ page: 1, limit: 10 })

    expect(response.statusCode).toBe(200)
    expect(response.body.docs).toHaveLength(1)
    expect(response.body.totalDocs).toBe(1)
    expect(response.body.limit).toBe(10)
    expect(response.body.totalPages).toBe(1)
    expect(response.body.page).toBe(1)
    expect(response.body.pagingCounter).toBe(1)
    expect(response.body.hasPrevPage).toBe(false)
    expect(response.body.hasNextPage).toBe(false)
    expect(response.body.prevPage).toBeNull()
    expect(response.body.nextPage).toBeNull()
  })

  test('Should validate limit not sent', async () => {
    const response = await request(app)
      .get('/breed')
      .query({ page: 1 })

    expect(response.statusCode).toBe(400)
    expect(response.body).toHaveLength(1)
    expect(response.body[0]?.path).toContain('limit')
    expect(response.body[0]?.message).toContain('limit is required')
  })
})
