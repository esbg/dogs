import { Request, Response } from 'express'
import { inject } from 'inversify'
import { controller, httpPost, request, response, interfaces, httpGet } from 'inversify-express-utils'
import { Breed } from '../core/entities'
import { BreedInteractor } from '../core/interactors'
import { Types } from '../ioc/types'
import { Validation } from '../middleware/validate'
import { createBreedSchema, listBreedSchema } from '../middleware/schema'
import { BreedCreationDto, BreedListDto } from '../core/dtos'

@controller("/breed")
export class BreedController implements interfaces.Controller {
  constructor(@inject(Types.types.breedInteractor) private breedInteractor: BreedInteractor) {
  }

  @httpPost('/', Validation.validate(createBreedSchema))
  async create(@request() req: Request, @response() res: Response): Promise<void> {
    const result: BreedCreationDto = await this.breedInteractor.create(new Breed(req.body.breed, req.body.url))
    res.json(result)
  }

  @httpGet('/', Validation.validate(listBreedSchema))
  async list(@request() req: Request, @response() res: Response): Promise<void> {
    const result: BreedListDto = await this.breedInteractor.list(Number(req.query.page), Number(req.query.limit))
    res.json(result)
  }

  @httpGet('/names')
  async names(@request() _: Request, @response() res: Response): Promise<void> {
    const result: any = await this.breedInteractor.names()
    res.json(result)
  }
}