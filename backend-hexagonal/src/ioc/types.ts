export class Types {
  static types = ({
    dbClient: Symbol('DbClient'),
    breedRepository: Symbol('breedRepository'),
    breedInteractor: Symbol('breedInteractor'),
    breedController: Symbol('breedController'),
    breedService: Symbol('breedService')
  })
}
