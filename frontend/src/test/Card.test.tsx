import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMemoryHistory } from 'history'
import Card from '../components/Card'
import { Breed } from '../types';

test('renders card', () => {
  const breed: Breed = {
    breed: 'doberman',
    url: 'https://images.dog.ceo/breeds/doberman/n02107142_5296.jpg'
  }

  render(<Card breed={breed} />)
  const nameElement = screen.getByText(/doberman/i)
  const urlElement = screen.getByAltText(breed.breed)
  expect(nameElement).toBeInTheDocument()
  expect(urlElement).toHaveAttribute('src', breed.url)
})
