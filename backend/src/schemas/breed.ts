import { object, string, TypeOf } from 'zod'

const payload = {
  body: object({
    breed: string({
      required_error: 'Breed is required'
    }),
    url: string({
      required_error: 'Image is required'
    })
  })
}

const query = {
  query: object({
    page: string({
      required_error: 'Page number is required'
    }),
    limit: string({
      required_error: 'Page limit is required'
    })
  })
}

export const createBreedSchema = object({
  ...payload
})

export const listBreedSchema = object({
  ...query
})

export type CreateBreedSchema = TypeOf<typeof createBreedSchema>
export type ListBreedSchema = TypeOf<typeof listBreedSchema>
