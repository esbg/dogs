import { useState } from 'react'
import { FormOption, Validation } from '../types'

export const useForm = <S extends Object>(options: FormOption<S>, validations: Validation[] = []) => {
  const [values, setValues] = useState<S>(options.initialValues)
  const [errors, setErrors] = useState<any>([])

  const handleChange = (evt: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    if (values.hasOwnProperty(evt.target.name)) {
      setValues({
        ...values,
        [evt.target.name]: evt.target.value
      })
    }
  }

  const validate = (): boolean => {
    const errorsValidation: any = validations.filter(v => (v.required && (!(values as any)[v.name] || (values as any)[v.name] === ''))).reduce((obj, item) =>  ({ ...obj, [item.name]: item.message }), {})
    setErrors(errorsValidation)
    return Object.keys(errorsValidation).length === 0;
  }

  const reset = (): void => {
    setValues(options.initialValues)
    setErrors({ })
  }

  return { values, errors, handleChange, validate, reset }
}
