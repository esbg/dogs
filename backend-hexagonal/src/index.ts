import { Application } from 'express'
import App from './app'
import { Config } from './utils/config'

(() => {
  const PORT = Config.env.port
  const app: Application = new App().getServer()
  app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`)
  })
})()