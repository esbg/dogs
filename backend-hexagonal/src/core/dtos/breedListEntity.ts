import { Breed } from "../entities"
import { BaseList } from "./baseList"

export class BreedListEntity extends BaseList<Breed> {
  constructor(hasNextPage: boolean, breeds: Breed[]) {
    super(hasNextPage, breeds)
  }
}
