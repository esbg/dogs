import mongoosePagination from 'mongoose-paginate-v2'
import { unmanaged, injectable, inject } from 'inversify'
import { Document, PaginateModel, PaginateResult, Schema, SchemaDefinition } from 'mongoose'
import { IGenericRepository } from '../../core/repositories';
import { Types } from '../../ioc/types';
import { DbClient } from './mongoConfig';
import { BaseList } from '../../core/dtos/baseList';

@injectable()
export class GenericRepository<TModel extends Document> implements IGenericRepository<TModel> {
  protected Model: PaginateModel<TModel>

  public constructor(
    @inject(Types.types.dbClient) dbClient: DbClient,
    @unmanaged() name: string,
    @unmanaged() schemaDefinition: SchemaDefinition
  ) {
    const schema = new Schema(schemaDefinition, { collection: name, timestamps: false });
    schema.plugin(mongoosePagination)
    this.Model = dbClient.model<TModel, PaginateModel<TModel>>(name, schema);
  }

  create = async (entity: TModel): Promise<TModel> => {
    return new this.Model(entity).save()
  }

  list = async (page: number, limit: number): Promise<BaseList<TModel>> => {
    const list: PaginateResult<TModel> = await this.Model.paginate({}, { page, limit })
    return new BaseList(list.hasNextPage, list.docs)
  }
}