const Card = ({ breed }: any) => {
  return (
    <li className="cards_item">
      <div className="card">
        <div className="card_image"><img src={breed.url} alt={breed.breed} /></div>
        <div className="card_content">
          <h2 className="card_title">{breed.breed}</h2>
        </div>
      </div>
    </li>
  )
}

export default Card
