import { Application } from 'express'
import request from 'supertest'
import App from '../app'

let app: Application
beforeAll(() => {
  app = new App().getServer()
})

describe('GET /breed?page={page}&limit={limit}', () => {
  test('Should get first page of breed list', async () => {
    const response = await request(app)
      .get('/breed')
      .query({ page: 1, limit: 10 })

    expect(response).not.toBeNull()
  })

  test('Should validate limit not sent', async () => {
    const response = await request(app)
    .get('/breed')
    .query({ page: 1 })

    expect(response.body).toHaveLength(1)
    expect(response.body[0]?.path).toContain('limit')
    expect(response.body[0]?.message).toContain('limit is required')
  })
})
