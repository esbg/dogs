import { IBreedService } from '../../core/services'
import { injectable } from 'inversify'

@injectable()
export class BreedService implements IBreedService {
  names = async (): Promise<any> => {
    const response: Response = await fetch('https://dog.ceo/api/breeds/list/all', { method: 'GET' })
    return response.json()
  }
}