import app from './app'
import { connect, disconnect } from './database/db.mongo.config'

const PORT = process.env.PORT ?? 4000

// eslint-disable-next-line @typescript-eslint/no-misused-promises
process.on('exit', async () => {
  await disconnect()
})

// eslint-disable-next-line @typescript-eslint/no-misused-promises
app.listen(PORT, async () => {
  console.log(`Server running on port ${PORT}`)
  await connect()
})
