import dotenv from 'dotenv'
import express from 'express'
import cors from 'cors'
import breedsRouter from './routes/breeds'

dotenv.config()
const app = express()
app.use(express.json())
app.use(cors())

app.get('/', (_, res) => {
  res.send('API running (▧ ͜ʖ▧)')
})

app.use('/breed', breedsRouter)

export default app
