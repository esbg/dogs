import { Breed } from '../entities'
import { BaseList } from './baseList'

export class BreedListDto extends BaseList<Breed> {
  constructor(hasNextPage: boolean, breedList: Breed[] | null) {
    super(hasNextPage, breedList)
  }
}
