import 'dotenv/config'

export class Config {
  static env = ({
    connectionString: process.env.CONNECTION_STRING ?? '',
    port: process.env.PORT ?? 4000
  })
}