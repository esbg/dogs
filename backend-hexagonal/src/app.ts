import 'reflect-metadata'
import bodyParser from 'body-parser'
import cors from 'cors'
import { IOCContainer } from './ioc/container'
import { InversifyExpressServer } from 'inversify-express-utils'
import { Application } from 'express'

export default class App {
  getServer = (): Application => {
    const container: IOCContainer = new IOCContainer().bindDependencies()
    return new InversifyExpressServer(container).setConfig((app) => {
      app.use(bodyParser.urlencoded({ extended: true }))
      app.use(bodyParser.json())
      app.use(cors())
    }).build()
  }
}
