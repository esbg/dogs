export class BaseList<T>  {
  constructor(hasNextPage: boolean, list: T[] | null){
    this.hasNextPage = hasNextPage
    this.list = list
  }

  hasNextPage: boolean
  list: T[] | null
}