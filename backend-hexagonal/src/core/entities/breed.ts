export class Breed {
  constructor(breed: string, url: string){
    this.breed = breed
    this.url = url
  }

  breed: string
  url: string
}
