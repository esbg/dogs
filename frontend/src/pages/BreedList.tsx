import debounce from 'just-debounce-it'
import { useContext, useEffect, useRef, useState, useCallback } from 'react'
import Card from '../components/Card'
import Context from '../context/Context'
import useNearScreen from '../hooks/useNearScreen'
import { listBreeds } from '../services/breed'
import { Breed } from '../types'

const PAGE_SIZE: number = 10

const ListPage = () => {
  const [list, setList] = useState<Breed[]>([])
  const [page, setPage] = useState<number>(1)
  const [hasNextPage, setHasNextPage] = useState<boolean>(false)
  const externalRef: any = useRef()
  const { isNearScreen } = useNearScreen({ externalRef })
  const { setLoading } = useContext(Context)

  const debouncHandleNextpage = useCallback(debounce(
    () => setPage(prevPage => prevPage + 1), 500
  ), [])

  useEffect(() => {
    if (isNearScreen && hasNextPage) debouncHandleNextpage()
  }, [debouncHandleNextpage, isNearScreen, hasNextPage])

  useEffect(() => {
    setLoading(true)
    listBreeds(page, PAGE_SIZE)
      .then(data => {
        setList(data.docs)
        setHasNextPage(data.hasNextPage)
      })
      .finally(() => setLoading(false))
  }, [])

  useEffect(() => {
    if (page === 1 || !hasNextPage) return
    setLoading(true)
    listBreeds(page, PAGE_SIZE)
      .then(data => {
        setList(prevDocs => prevDocs.concat(data.docs))
        setHasNextPage(data.hasNextPage)
      })
      .finally(() => setLoading(false))
  }, [page])

  return (<>
    <div className='cards-container'>
      <ul className="cards">
        {
          list.map((b, i) => <Card breed={b} key={i} />)
        }
      </ul>
    </div>
    <div id='visor' ref={externalRef}></div>
  </>)
}

export default ListPage
