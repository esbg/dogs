import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import Header from './components/Header'
import AddBreedPage from './pages/AddBreed'
import HomePage from './pages/Home'
import BreedListPage from './pages/BreedList'
import { Provider } from './context/Context'
import './App.css'
import { ToastContainer } from 'react-toastify'

function App() {
  return (
    <Provider>
      <BrowserRouter>
        <Header />
        <ToastContainer />
        <Routes>
          <Route path='/' element={<HomePage />} />
          <Route path='/list' element={<BreedListPage />} />
          <Route path='/add' element={<AddBreedPage />} />
          <Route path='*' element={<Navigate to='/' />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App
