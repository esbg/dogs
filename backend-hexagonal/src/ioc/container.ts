import { Container } from 'inversify'
import { BaseController, BreedController } from '../controller'
import { BreedInteractor } from '../core/interactors'
import { IGenericRepository } from '../core/repositories'
import { IBreedService } from '../core/services'
import { BreedRepository, BreedModel } from '../dataSource/repository/breedRepository'
import { BreedService } from '../dataSource/service/breedServices'
import { DbClient } from '../dataSource/repository/mongoConfig'
import { Types } from './types'
import mongoose from 'mongoose'
import { Config } from '../utils/config'

export class IOCContainer extends Container {
  public bindDependencies() {
    this.bind<DbClient>(Types.types.dbClient).toDynamicValue(() => {
          mongoose.connect(Config.env.connectionString)
          return mongoose
      }).inSingletonScope()
    this.bind<IBreedService>(Types.types.breedService).to(BreedService).inRequestScope()
    this.bind<IGenericRepository<BreedModel>>(Types.types.breedRepository).to(BreedRepository).inSingletonScope()
    this.bind<BreedInteractor>(Types.types.breedInteractor).to(BreedInteractor).inRequestScope()
    this.bind<BreedController>(Types.types.breedController).to(BreedController).inRequestScope()
    this.bind<BaseController>(Types.types.breedController).to(BaseController).inRequestScope()
    return this
  }
}
