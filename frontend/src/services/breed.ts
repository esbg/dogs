import { Breed } from "../types"

const { REACT_APP_API } = process.env

export const addBreed = (breed: Breed) => fetch(`${REACT_APP_API}/breed`, {
  method: 'POST',
  headers: getHeaders(),
  body: JSON.stringify(breed)
}).then(handleResponse)

export const listBreeds = (page: number, limit: number) => fetch(`${REACT_APP_API}/breed?page=${page}&limit=${limit}`, {
  method: 'GET',
  headers: getHeaders()
}).then(handleResponse)

export const listBreedNames = () => fetch(`${REACT_APP_API}/breed/names`, {
  method: 'GET',
  headers: getHeaders()
}).then(handleResponse)

const getHeaders = () => ({
  'Content-Type': 'application/json'
})

const handleResponse = (res: Response) => {
  if (res.status === 400) throw new Error('Error fetching from API')
  return res.json()
};