import { Request, Response } from 'express'
import { interfaces, controller, httpGet, request, response } from "inversify-express-utils";

@controller("/")
export class BaseController implements interfaces.Controller {
  @httpGet('/')
  async list(@request() _: Request, @response() res: Response): Promise<void> {
    res.send('API running (▧ ͜ʖ▧)')
  }
}