import dotenv from 'dotenv'
dotenv.config()

export const connection = {
  connectionString: process.env.CONNECTION_STRING
}

export default connection
