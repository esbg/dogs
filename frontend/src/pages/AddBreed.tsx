import React, { useContext, useEffect, useState } from 'react'
import Context from '../context/Context'
import { useForm } from '../hooks/useForm'
import { addBreed, listBreedNames } from '../services/breed'
import { Breed, Validation } from '../types'
import { toast } from 'react-toastify';

const validations: Validation[] = [
  {
    name: 'breed',
    required: true,
    message: 'Breed is required'
  },
  {
    name: 'url',
    required: true,
    message: 'Url is required'
  }
]

const AddBreedPage = () => {
  const [breedNames, setBreedNames] = useState<string[]>([])
  const { values, errors, handleChange, validate, reset } = useForm<Breed>({ initialValues: { breed: '', url: '' } }, validations)
  const { setLoading } = useContext(Context)

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      const data: string[] = await listBreedNames()
      setBreedNames(data)
      setLoading(false)
    }

    fetchData()
  }, [])

  const handleSubmit = async (evt: React.FormEvent<HTMLFormElement>) => {
    evt.preventDefault()
    if (validate()) {
      await addBreed(values)
      toast.success('Breed created succesfully', { position: toast.POSITION.TOP_RIGHT })
      reset()
    }
  }

  return (<>
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="panel panel-primary">
            <div className="panel-heading">
              <h3 className="panel-title text-center">Add breed</h3>
            </div>
            <div className="panel-body">
              <form className="form-horizontal" onSubmit={handleSubmit}>
                <div className="form-group">
                  <label className="col-sm-3 control-label">Breed</label>
                  <div className="col-sm-12">
                    <select name='breed' value={values.breed} onChange={handleChange} className='form-control'>
                      <option value='' key={'select'}>Select</option>
                      {
                        breedNames.map(b => <option value={b} key={b}>{b}</option>)
                      }
                    </select>
                    {errors.breed && <p className="form-text text-danger">{errors.breed}</p>}
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-sm-3 control-label">URL</label>
                  <div className="col-sm-12">
                    <input type='text' name='url' value={values.url} onChange={handleChange} className='form-control' />
                    {errors.url && <div className="form-text text-danger">{errors.url}</div>}
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-sm-10">
                    <input type='submit' value='Send' className='btn btn-dark btn-block' />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>)
}

export default AddBreedPage
