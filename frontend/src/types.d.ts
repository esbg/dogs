export class Breed {
  breed: string
  url: string
}

export class FormOption<S> {
  initialValues: S
  validations?: Validation[]
}

export class Validation {
  name: string
  required: boolean = false
  message?: string
}

export class ListResult {
  docs: Breed[]
  hasNextPage: boolean
}

export type ContextType = {
  loading: boolean
  setLoading: (loading: boolean) => void
}