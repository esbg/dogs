import express from 'express'
import { addBreedHandler, listBreedHandler, listBreedNamesHandler } from '../controller/breedController'
import validate from '../middleware/validate'
import { createBreedSchema, listBreedSchema } from '../schemas/breed'
const router = express.Router()

router.get('/', [validate(listBreedSchema)], listBreedHandler)

router.get('/names', (req, res) => listBreedNamesHandler(req, res))

router.post('/', [validate(createBreedSchema)], addBreedHandler)

export default router
