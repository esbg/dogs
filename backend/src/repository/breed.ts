import { PaginateResult } from 'mongoose'
import { BreedModel } from '../model/breed'
import { Breed } from '../types'

export const createBreed = async (breed: Breed): Promise<Breed> => {
  try {
    return await BreedModel.create(breed)
  } catch (e) {
    throw new Error(e.message)
  }
}

export const listBreed = async (page: number, limit: number): Promise<PaginateResult<Breed>> => {
  try {
    return await BreedModel.paginate({}, { page, limit })
  } catch (e) {
    throw new Error(e.message)
  }
}
