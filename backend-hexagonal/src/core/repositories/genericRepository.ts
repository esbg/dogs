import { BaseList } from '../dtos/baseList'

export interface IGenericRepository<T> {
  create(breed: T): Promise<T>
  list(page: number, limit: number): Promise<BaseList<T>>
}
